import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import router from '@/router'
 const baseURL ='http://localhost:8888/api/private/v1/'
axios.defaults.baseURL = baseURL
// axios.defaults.baseURL ='http://192.168.1.119:8888/api/private/v1/'
// export default function http(url,data,method,params){
//     return new Promise((resolve,reject) => {
//         axios({
//             url,
//             data,
//             method,
//             params
//         })
//         .then(res=>{
//             if(res.data.meta.status >= 200 && res.data.meta.status < 300){
//                 resolve(res.data)
//             }else{
//                 Message.error(res.data.meta.msg);
//                 reject(res.data.meta)
//             }
//         }).catch(err => {
//             Message.error(err)
//             reject(err)
//         })
//     })
        
// }
axios.interceptors.request.use(function (config) {
    
    if(store.state.token) {
        config.headers.Authorization = store.state.token
    }
    return config;
}, function (error) {
    
    return Promise.reject(error);
});


axios.interceptors.response.use(function (response) {
    
    if(response.data.meta.msg==='无效token') {
        router.replace({
            name: 'login'
        })
    }
    return response;
}, function (error) {
    
    return Promise.reject(error);
});

export const http=  async function(url, data, method, params) {
    try {
        let res = await axios({
            url,
            data,
            method: method ? method.toUpperCase() : 'GET',
            params
        })
        if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
            return Promise.resolve(res.data)
        }else {
            Message.error(res.data.meta.msg)
            return Promise.reject(res.data.meta)
        }

    }catch(err) {
        Message.error(err)
        return Promise.reject(err)
    }
}

export const _baseUrl = baseURL