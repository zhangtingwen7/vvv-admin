import Vue from 'vue'
import Router from 'vue-router'
import LogIn from '@/views/LogIn'
import Home from '@/views/Home'
import store from '@/store'
import Users from '@/views/users/users'
import Roles from '@/views/rights/Roles'
import Rights from '@/views/rights/Rights'
import Params from '@/views/goods/Params'
import CateGories from '@/views/goods/CateGories'
import Goods from '@/views/goods/Goods'
import AddGoods from '@/views/goods/AddGoods'
import Reports from '@/views/data/Reports'
import Orders from '@/views/orders/Orders'

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)

const routes = new Router({
  routes: [
    {
      path:'/',
      redirect:'/login'
    },
    {
      path: '/login',
      name:'login',
      component:LogIn,
      meta:{requiresAuth: true}
    },
    {
      path: '/home',
      name:'home',
      component:Home,
      children:[
        {
          path: 'users',
          name: 'users',
          component: Users
        },
        {
          path: 'roles',
          name: 'roles',
          component: Roles
        },{
          path: 'goods',
          name: 'goods',
          component: Goods
        },
        {
          path: 'goodsadd',
          name: 'goodsadd',
          component: AddGoods
        },{
          path: 'rights',
          name: 'rights',
          component: Rights
        },{
          path: 'params',
          name: 'params',
          component: Params
        },
        {
          path: 'categories',
          name: 'categories',
          component: CateGories
        },
        {
          path: 'reports',
          name: 'reports',
          component: Reports
        },
        {
          path: 'orders',
          name: 'orders',
          component: Orders
        }
      ]
    }
  ],
  linkActiveClass: 'actived-class'
})

routes.beforeEach((to, from, next) => {
  if(!to.meta.requiresAuth) {
    if(store.state.token){
      next()
    }else {
      next({
        path: '/login'
      })
    }
  }else {
    next()
  }
})

export default routes
