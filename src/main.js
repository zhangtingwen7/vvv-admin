
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import 'reset-css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import {http} from './http'
import filters from './filters'

import echarts from 'echarts'
import VCharts from 'v-charts'
Vue.prototype.$echarts = echarts


Vue.use(echarts)
Vue.use(VCharts)
Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.prototype.$http=http

Object.keys(filters).forEach(item => {
  Vue.filter(item, filters[item])
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
